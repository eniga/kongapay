﻿using System;
using System.Data;
using KongaPay.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace KongaPay.Repositories
{
    public class WalletRepository : DapperRepository<Wallet>
    {
        public WalletRepository(IDbConnection connection, ISqlGenerator<Wallet> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
