﻿using System;
using System.Data;
using KongaPay.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace KongaPay.Repositories
{
    public class PaymentRepository : DapperRepository<Payment>
    {
        public PaymentRepository(IDbConnection connection, ISqlGenerator<Payment> sqlGenerator)
            : base(connection, sqlGenerator)
        {
        }
    }
}
