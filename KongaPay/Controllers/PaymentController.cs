﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KongaPay.Models;
using KongaPay.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KongaPay.Controllers
{
    [Route("api/[controller]")]
    public class PaymentController : Controller
    {
        KongaService service;

        public PaymentController(IConfiguration configuration, ILogger<KongaService> log)
        {
            service = new KongaService(configuration, log);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(PaymentResponse))]
        public ActionResult MakePayment([FromBody]ProcessPaymentRequest request)
        {
            var response = service.ProcessPayment(request);
            if (response != null)
            {
                if (response.code == "00")
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }

        [HttpGet("requery/{merchant_reference}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentRequeryResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ScheduleErrorResponse))]
        public ActionResult Requery(string merchant_reference)
        {
            var response = service.Requery(merchant_reference);
            if (response != null)
            {
                if (response.GetType() == typeof(PaymentRequeryResponse))
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }

        [HttpPost("refund")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RefundResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(RefundResponse))]
        public ActionResult Refund([FromBody]PaymentRefundRequest request)
        {
            var response = service.Refund(request);
            if (response != null)
            {
                if(response.status == "success")
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }

        [HttpPost("invoice")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoiceSuccessResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(InvoiceErrorResponse))]
        public ActionResult CreateInvoice([FromBody]Invoice request)
        {
            var response = service.CreateInvoice(request);
            if (response != null)
            {
                if (response.GetType() == typeof(InvoiceSuccessResponse))
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
