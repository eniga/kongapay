﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KongaPay.Models;
using KongaPay.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KongaPay.Controllers
{
    [Route("api/[controller]")]
    public class ScheduleController : Controller
    {
        KongaService service;

        public ScheduleController(IConfiguration configuration, ILogger<KongaService> log)
        {
            service = new KongaService(configuration, log);
        }

        // GET api/values/5
        [HttpGet("{reference}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ScheduleSuccessResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ScheduleErrorResponse))]
        public ActionResult GetSchedule(string reference)
        {
            var response = service.GetSchedule(reference);
            if (response != null)
            {
                if (response.GetType() == typeof(ScheduleSuccessResponse))
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ScheduleSuccessResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ScheduleErrorResponse))]
        public ActionResult CreateSchedule([FromBody]ScheduleRequest request)
        {
            var response = service.CreateSchedule(request);
            if (response != null)
            {
                if (response.GetType() == typeof(ScheduleSuccessResponse))
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
