﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KongaPay.Models;
using KongaPay.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KongaPay.Controllers
{
    [Route("api/[controller]")]
    public class WalletController : Controller
    {
        KongaService service;

        public WalletController(IConfiguration configuration, ILogger<KongaService> log)
        {
            service = new KongaService(configuration, log);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(WalletSuccessResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(WalletErrorResponse))]
        public ActionResult CreateWallet([FromBody]WalletRequest request)
        {
            var response = service.CreateWallet(request);
            if (response != null)
            {
                if (response.GetType() == typeof(WalletSuccessResponse))
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
