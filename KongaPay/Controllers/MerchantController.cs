﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KongaPay.Models;
using KongaPay.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KongaPay.Controllers
{
    [Route("api/[controller]")]
    public class MerchantController : Controller
    {
        KongaService service;

        public MerchantController(IConfiguration configuration, ILogger<KongaService> log)
        {
            service = new KongaService(configuration ,log);
        }

        [HttpGet("Banks")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Banks))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BankErrorResponse))]
        public ActionResult GetBanks()
        {
            var response = service.GetBanks();
            if (response != null)
            {
                if (response.GetType() == typeof(Banks))
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }

        [HttpGet("account/{AccountNumber}/bank/{BankCode}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ScheduleErrorResponse))]
        public ActionResult GetAccountName(string AccountNumber, string BankCode)
        {
            var response = service.GetAccountName(AccountNumber, BankCode);
            if (response != null)
            {
                if (response.GetType() == typeof(AccountResponse))
                {
                    return Ok(response);
                }
                else
                {
                    return BadRequest(response);
                }
            }
            else
            {
                return BadRequest(response);
            }
        }
    }
}
