﻿using System;
using KongaPay.Models.FlexConnect;
using KongaPay.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace KongaPay.Services
{
    public class FlexConnectService
    {
        private string ApiUrl;
        readonly ILogger<KongaService> logger;

        public FlexConnectService(IConfiguration configuration, ILogger<KongaService> log)
        {
            ApiUrl = configuration.GetValue<string>("FlexConnectConfig:ApiUrl");
            logger = log;
        }

        public FlexAccount GetBalance(string AccountNumber)
        {
            FlexAccount response = new FlexAccount();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest($"balance/{AccountNumber}", Method.GET);
                var result = client.Execute<FlexAccount>(req);
                if (result.IsSuccessful)
                {
                    response = result.Data;
                }
                else
                {
                    response = null;
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("FlexConnect:: GetBalance returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public FlexAccount GetBalanceByBvn(string AccountNumber)
        {
            FlexAccount response = new FlexAccount();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest($"balance/bvn/{AccountNumber}", Method.GET);
                var result = client.Execute<FlexAccount>(req);
                if (result.IsSuccessful)
                {
                    response = result.Data;
                }
                else
                {
                    response = null;
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("FlexConnect:: GetBalance returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public StatCode FundTransfer(FundTransferRequest request)
        {
            StatCode response = new StatCode();
            response.TranRef = request.Body.TransRef;
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest("fundtransfer", Method.POST);
                req.AddJsonBody(request);
                var result = client.Execute<StatCode>(req);
                if (result.IsSuccessful)
                {
                    response = result.Data;
                }
                else
                {
                    response = result.Data;
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("FlexConnect:: GetBalance returns {0}", log);
            }
            catch (Exception ex)
            {
                response.ResponseCode = "96";
                response.ResponseMsg = "System malfunction";
                logger.LogError(ex.Message);
            }
            return response;
        }

        public StatCode FundTransferTSQ(string TransRef)
        {
            StatCode response = new StatCode();
            response.TranRef = TransRef;
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest($"fundtransfer/tsq/{TransRef}", Method.GET);
                var result = client.Execute<StatCode>(req);
                if (result.IsSuccessful)
                {
                    response = result.Data;
                }
                else
                {
                    response = null;
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("FlexConnect:: GetBalance returns {0}", log);
            }
            catch (Exception ex)
            {
                response.ResponseCode = "96";
                response.ResponseMsg = "System malfunction";
                logger.LogError(ex.Message);
            }
            return response;
        }
    }
}
