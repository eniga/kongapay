﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using KongaPay.Models;
using KongaPay.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace KongaPay.Services
{
    public class WalletService
    {
        WalletRepository repo;
        readonly ILogger<KongaService> logger;

        public WalletService(IConfiguration configuration, ILogger<KongaService> log)
        {
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<Wallet>(SqlProvider.MSSQL);
            repo = new WalletRepository(conn, generator);
            logger = log;
        }

        public IEnumerable<Wallet> List()
        {
            IEnumerable<Wallet> result;
            try
            {
                result = repo.FindAll();
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public Wallet GetById(int Id)
        {
            Wallet result;
            try
            {
                result = repo.Find(x => x.id == Id);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public int GetId(Wallet wallet)
        {
            int result;
            try
            {
                return repo.Find(x => x.email == wallet.email && x.request_id == wallet.request_id).id;
            }
            catch (Exception ex)
            {
                result = 0;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public bool Add(Wallet item)
        {
            try
            {
                return repo.Insert(item);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }
        }

        public bool Update(Wallet item)
        {
            try
            {
                return repo.Update(item);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                return repo.Delete(new Wallet() { id = id });
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
