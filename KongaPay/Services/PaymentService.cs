﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using KongaPay.Models;
using KongaPay.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace KongaPay.Services
{
    public class PaymentService
    {
        PaymentRepository repo;
        readonly ILogger<KongaService> logger;

        public PaymentService(IConfiguration configuration, ILogger<KongaService> log)
        {
            var conn = new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
            var generator = new SqlGenerator<Payment>(SqlProvider.MSSQL);
            repo = new PaymentRepository(conn, generator);
            logger = log;
        }

        public IEnumerable<Payment> List()
        {
            IEnumerable<Payment> result;
            try
            {
                result = repo.FindAll();
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public Payment GetById(int Id)
        {
            Payment result;
            try
            {
                result = repo.Find(x => x.id == Id);
            }
            catch (Exception ex)
            {
                result = null;
                logger.LogError(ex.Message);
            }
            return result;
        }

        public bool Add(Payment item)
        {
            try
            {
                return repo.Insert(item);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }
        }

        public bool Update(Payment item)
        {
            try
            {
                return repo.Update(item);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                return repo.Delete(new Payment() { id = id });
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
