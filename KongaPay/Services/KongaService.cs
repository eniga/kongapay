﻿using System;
using KongaPay.Models;
using KongaPay.Models.FlexConnect;
using KongaPay.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace KongaPay.Services
{
    public class KongaService
    {
        private static string ApiUrl;
        private static string ApiKey;
        private FlexConnectService flexConnectService;
        private IConfiguration config;
        readonly ILogger<KongaService> logger;

        public KongaService(IConfiguration configuration, ILogger<KongaService> log)
        {
            ApiUrl = configuration.GetValue<string>("KongaPayConfig:ApiUrl");
            ApiKey = configuration.GetValue<string>("KongaPayConfig:ApiKey");
            flexConnectService = new FlexConnectService(configuration, log);
            config = configuration;
            logger = log;
        }

        public Object GetBanks()
        {
            Object response = new Object();
            logger.LogDebug("Get List of banks from KongaPay");
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest("merchant/banks", Method.GET);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<Banks>(result.Content);
                    if(response.GetType() != typeof(Banks))
                    {
                        response = JsonConvert.DeserializeObject<BankErrorResponse>(result.Content);
                    }
                }
                else
                {
                    response = JsonConvert.DeserializeObject<BankErrorResponse>(result.Content);
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: GetBank returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public Object GetAccountName(string AccountNumber, string BankCode)
        {
            Object response = new object();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest($"merchant/account/{AccountNumber}/bank/{BankCode}", Method.GET);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<AccountResponse>(result.Content);
                }
                else
                {
                    response = JsonConvert.DeserializeObject<ScheduleErrorResponse>(result.Content);
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: GetAccountName returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public Object GetSchedule(string reference)
        {
            Object response = new object();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest($"recur/pay/schedule/{reference}", Method.GET);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<ScheduleSuccessResponse>(result.Content);
                }
                else
                {
                    response = JsonConvert.DeserializeObject<ScheduleErrorResponse>(result.Content);
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: GetSchedule returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public Object Requery(string merchant_reference)
        {
            Object response = new object();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest($"payment/requery/{merchant_reference}", Method.GET);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<PaymentRequeryResponse>(result.Content);
                }
                else
                {
                    response = JsonConvert.DeserializeObject<ScheduleErrorResponse>(result.Content);
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: Requery returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public RefundResponse Refund(PaymentRefundRequest request)
        {
            RefundResponse response = new RefundResponse();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest("payment/refund", Method.POST);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                req.AddJsonBody(request);
                var result = client.Execute<RefundResponse>(req);
                if (result.IsSuccessful)
                {
                    response = result.Data;
                }
                else
                {
                    response.status = "failed";
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: Refund returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public Object CreateWallet(WalletRequest request)
        {
            Object response = new Object();
            
            try
            {
                var service = new WalletService(config, logger);
                Wallet wallet = (Wallet)request;
                service.Add(wallet);
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest("user/wallet", Method.POST);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                req.AddJsonBody(request);
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<WalletSuccessResponse>(result.Content);
                }
                else
                {
                    response = JsonConvert.DeserializeObject<WalletErrorResponse>(result.Content);
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: CreateWallet returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public Object CreateSchedule(ScheduleRequest request)
        {
            Object response = new Object();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest("recur/pay/schedule", Method.POST);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                req.AddJsonBody(request);
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<ScheduleSuccessResponse>(result.Content);
                }
                else
                {
                    response = JsonConvert.DeserializeObject<ScheduleErrorResponse>(result.Content);
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: CreateSchedule returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public Object CreateInvoice(Invoice request)
        {
            Object response = new Object();
            try
            {
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest("merchant/payment/invoice", Method.POST);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                req.AddJsonBody(request);
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<InvoiceSuccessResponse>(result.Content);
                }
                else
                {
                    response = JsonConvert.DeserializeObject<InvoiceErrorResponse>(result.Content);
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: CreateInvoice returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public PaymentResponse MakePayment(PaymentRequest request)
        {
            PaymentResponse response = new PaymentResponse();
            try
            {
                string hash = SHAUtility.GenerateSHA512String(request.amount + "|" + ApiKey + "|" + request.reference);
                request.hash = hash;
                request.merchantKey = ApiKey;
                RestClient client = new RestClient(ApiUrl);
                RestRequest req = new RestRequest("paymentgateway", Method.POST);
                req.AddHeader("Authorization", "Bearer " + ApiKey);
                req.AddHeader("Content-Type", "application/json");
                req.AddJsonBody(request);

                var result = client.Execute<PaymentResponse>(req);
                response = result.Data;
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: MakePayment returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }

        public PaymentResponse ProcessPayment(ProcessPaymentRequest request)
        {
            PaymentResponse response = new PaymentResponse();
            
            try
            {
                var service = new PaymentService(config, logger);
                Payment payment = new Payment()
                {
                    amount = request.amount,
                    callback = request.callback,
                    debitaccountnumber = request.debitaccountnumber,
                    description = request.description,
                    email = request.email,
                    enableFrame = request.enableFrame,
                    firstname = request.firstname,
                    hash = request.hash,
                    merchantId = request.merchantId,
                    lastname = request.lastname,
                    merchantKey = request.merchantKey,
                    metadata = request.metadata,
                    phone = request.phone,
                    reference = request.reference,
                    selectedChannelIdentifier = request.selectedChannelIdentifier,
                    date_created = DateTime.Now,
                    date_modified = DateTime.Now
                };
                service.Add(payment);
                // Debit Customer before calling Konga service
                string accountnumber = request.debitaccountnumber;

                // Get flexconnect config
                var section = config.GetSection("FlexConnectConfig");
                FlexConnectConfig flexConnectConfig = section.Get<FlexConnectConfig>();

                //FlexConnectConfig flexConnectConfig =  config.GetValue<FlexConnectConfig>("FlexConnectConfig");
                // Name enquiry
                FlexAccount account = flexConnectService.GetBalance(accountnumber);
                if (account == null)
                {
                    response.code = "96";
                    response.message = "Invalid Account";
                    return response;
                }

                // Defining Transaction body
                FundTransferRequestBody transferRequestBody = new FundTransferRequestBody()
                {
                    amount = request.amount,
                    Channel = flexConnectConfig.Channel,
                    CreditAccount = flexConnectConfig.CreditAccount,
                    CurrencyCode = flexConnectConfig.CurrencyCode,
                    isCharge = flexConnectConfig.isCharge.ToString(),
                    Narration = flexConnectConfig.Narration.Replace("[CustomerName]", account.CustomerName),
                    SourceCode = flexConnectConfig.SourceCode,
                    TransCode = flexConnectConfig.TransCode,
                    TransRef = request.reference,
                    TrnDate = DateTime.Now,
                    UserID = flexConnectConfig.UserID,
                    ValueDate = DateTime.Now,
                    DebitAccount = account.CustAccNo
                };
                FundTransferRequest fundTransferRequest = new FundTransferRequest()
                {
                    Body = transferRequestBody
                };

                // Pass Debit
                var debitResponse = flexConnectService.FundTransfer(fundTransferRequest);
                if (debitResponse.ResponseCode == "00")
                {
                    // Call Konga Service
                    response = MakePayment(request);
                    if (response.code == "00")
                    {
                        // Pass reversal
                        fundTransferRequest.Body.amount = decimal.Negate(fundTransferRequest.Body.amount);
                        flexConnectService.FundTransfer(fundTransferRequest);
                    }
                }
                string log = Helper.ObjectToJson(response);
                logger.LogDebug("Konga Pay:: ProcessPayment returns {0}", log);
            }
            catch (Exception ex)
            {
                response = null;
                logger.LogError(ex.Message);
            }
            return response;
        }
    }
}
