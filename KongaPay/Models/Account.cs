﻿using System;
namespace KongaPay.Models
{
    public class AccountResponse
    {
        public string status { get; set; }
        public AccountSuccessResponse data { get; set; }
    }

    public class AccountSuccessResponse
    {
        public string name { get; set; }
    }
}
