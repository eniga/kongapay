﻿using System;
using System.Collections.Generic;

namespace KongaPay.Models
{
    public class Banks
    {
        public string status { get; set; }
        public IEnumerable<BankMetaData> data { get; set; }
    }

    public class BankMetaData
    {
        public string code { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string is_mandate { get; set; }
        public string nip_bank_code { get; set; }
        public string is_bvn_mandate { get; set; }
    }

    public class BankErrorResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public string code { get; set; }
    }
}
