﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace KongaPay.Models
{
    [Table("tbl_konga_payments")]
    public class Payment : ProcessPaymentRequest
    {
        [Key, Identity]
        public int id { get; set; }
        [UpdatedAt]
        public DateTime date_created { get; set; }
        [UpdatedAt]
        public DateTime date_modified { get; set; }
    }

    public class PaymentRequest
    {
        public decimal amount { get; set; }
        public string description { get; set; }
        public string hash { get; set; }
        public string merchantKey { get; set; }
        public string email { get; set; }
        public string merchantId { get; set; }
        public string reference { get; set; }
        public string callback { get; set; }
        public IEnumerable<PaymentMetaData> metadata { get; set; }
        public bool enableFrame { get; set; }
        public string phone { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string selectedChannelIdentifier { get; set; }
    }

    public class ProcessPaymentRequest: PaymentRequest
    {
        public string debitaccountnumber { get; set; }
    }

    public class PaymentMetaData
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    [Table("tbl_konga_meta_data")]
    public class TblPaymentMetaData : PaymentMetaData
    {
        [Key, Identity]
        public int id { get; set; }
    }

    public class PaymentResponse
    {
        public string code { get; set; }
        public string message { get; set; }
        public string status { get; set; }
        public string merchant_refernce { get; set; }
    }

    public class PaymentRequeryResponse
    {
        public string status { get; set; }
        public PaymentRequeryData data { get; set; }
    }

    public class PaymentRequeryData
    {
        public PaymentCharge charge { get; set; }
        public PaymentInfo payment_info { get; set; }
        public IEnumerable<PaymentMetaData> metadata { get; set; }
    }

    public class PaymentCharge
    {
        public decimal amount { get; set; }
        public decimal originalAmount { get; set; }
        public string identifier { get; set; }
        public string createdAt { get; set; }
        public string merchantReference { get; set; }
        public string customerId { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public bool successful { get; set; }
    }

    public class PaymentInfo
    {
        public string channel { get; set; }
        public string card_pan { get; set; }
        public string card_scheme { get; set; }
        public string mobile_number { get; set; }
        public string token { get; set; }
    }

    public class PaymentRefundRequest
    {
        public decimal amount { get; set; }
        public int reference { get; set; }
    }
}
