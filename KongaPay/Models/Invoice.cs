﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KongaPay.Models
{
    public class Invoice
    {
        public string sender_name { get; set; }
        public string receiver_name { get; set; }
        public decimal total_amount { get; set; }
        public decimal total_due { get; set; }
        public decimal total_paid { get; set; }
        public string email { get; set; }
        [DataType(DataType.PhoneNumber)]
        public int phone { get; set; }
        public string comment { get; set; }
        public IEnumerable<InvoiceItem> items { get; set; }
        public IEnumerable<InvoiceTax> tax { get; set; }
        public InvoiceDiscount discount { get; set; }
        public string due_date { get; set; }
        public string reference { get; set; }
        public int shipping { get; set; }
    }

    public class InvoiceItem
    {
        public string name { get; set; }
        public int quantity { get; set; }
        public decimal amount { get; set; }
    }

    public class InvoiceTax
    {
        public string name { get; set; }
        public decimal amount { get; set; }
    }

    public class InvoiceDiscount
    {
        public string type { get; set; }
    }

    public class InvoiceSuccessResponse
    {
        public string status { get; set; }
        public InvoiceMetaData data { get; set; }
    }

    public class InvoiceErrorResponse
    {
        public string status { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class InvoiceMetaData
    {
        public string invoice_number { get; set; }
    }
}
