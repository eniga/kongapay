﻿using System;
namespace KongaPay.Models.FlexConnect
{
    public class FlexAccount
    {
        public string CustAccNo { get; set; }
        public string CustAccName { get; set; }
        public decimal AccBanlance { get; set; }
        public string ResponseCode { get; set; }
        public string CodeDesc { get; set; }
        public string CustomerName { get; set; }
        public string EmailAdd { get; set; }
        public string PhoneNo { get; set; }
        public string BVN { get; set; }
        public string acStat { get; set; }
        public string CustomerNo { get; set; }
    }

    public class FundTransferRequest
    {
        public FundTransferRequestBody Body { get; set; }
    }

    public class FundTransferRequestBody
    {
        public string TransRef { get; set; }
        public string DebitAccount { get; set; }
        public string CreditAccount { get; set; }
        public string CurrencyCode { get; set; }
        public decimal amount { get; set; }
        public DateTime TrnDate { get; set; }
        public DateTime ValueDate { get; set; }
        public string Narration { get; set; }
        public string Channel { get; set; }
        public string TransCode { get; set; }
        public string SourceCode { get; set; }
        public string UserID { get; set; }
        public string isCharge { get; set; }
    }

    public class StatCode
    {
        public string ResponseCode { get; set; }
        public string CodeDesc { get; set; }
        public string TranRef { get; set; }
        public decimal amount { get; set; }
        public string CurrencyCode { get; set; }
        public string CreditAcNo { get; set; }
        public string DebitAcNo { get; set; }
        public string ProcessStat { get; set; }
        public string ResponseMsg { get; set; }
        public DateTime TrnDate { get; set; }
    }

    public class FlexConnectConfig
    {
        public string ApiUrl { get; set; }
        public string UserID { get; set; }
        public string SourceCode { get; set; }
        public string Channel { get; set; }
        public string TransCode { get; set; }
        public bool isCharge { get; set; }
        public string CreditAccount { get; set; }
        public string Narration { get; set; }
        public string CurrencyCode { get; set; }
    }
}
