﻿using System;
using System.Collections.Generic;

namespace KongaPay.Models
{
    public class Schedule
    {
        public Schedule()
        {
        }
    }

    public class ScheduleRequest
    {
        public string frequency { get; set; }
        public string periodic_debit_amount { get; set; }
        public int cycle_count { get; set; }
        public string reference { get; set; }
        public decimal total_debit_amount { get; set; }
        public string payment_token { get; set; }
        public string cycle_debits { get; set; }
    }

    public class ScheduleSuccessResponse
    {
        public string status { get; set; }
        public ScheduleMetaData data { get; set; }
    }

    public class ScheduleMetaData
    {
        public string message { get; set; }
    }

    public class ScheduleErrorResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public string code { get; set; }
    }

    public class ScheduleData
    {
        public string frequency { get; set; }
        public string debit_amont { get; set; }
        public string full_amount { get; set; }
        public string cycles { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
        public string product_reference { get; set; }
        public string last_payment_date { get; set; }
        public string next_due_date { get; set; }
        public string payments_completed { get; set; }
        public IEnumerable<ScheduleTransactions> transactions { get; set; }
    }

    public class ScheduleTransactions
    {
        public string status { get; set; }
        public string amount { get; set; }
        public string transaction_reference { get; set; }
        public string response_description { get; set; }
    }
}
