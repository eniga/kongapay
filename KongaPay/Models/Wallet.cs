﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace KongaPay.Models
{
    [Table("tbl_konga_wallets")]
    public class Wallet : WalletRequest
    {
        [Key, Identity]
        public int id { get; set; }
        public string user_id { get; set; }
        public string request_id { get; set; }
        [UpdatedAt]
        public DateTime date_created { get; set; }
        [UpdatedAt]
        public DateTime date_modified { get; set; }
    }

    public class WalletRequest
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public int account_type_id { get; set; }
        [DataType(DataType.Date)]
        public string date_of_birth { get; set; }
        public int bank_code { get; set; }
        [DataType(DataType.CreditCard)]
        public string card_number { get; set; }
        [DataType(DataType.Date)]
        public string expiry_date { get; set; }
        public int cvv { get; set; }
        public string card_pin { get; set; }
        public string account_number { get; set; }
        public string account_name { get; set; }
    }

    public class WalletSuccessResponse
    {
        public string status { get; set; }
        public WalletMetaData data { get; set; }
    }

    public class WalletMetaData
    {
        public string user_id { get; set; }
        public string email { get; set; }
        public string request_id { get; set; }
    }

    public class WalletErrorResponse
    {
        public string status { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string terminal { get; set; }
    }
}
